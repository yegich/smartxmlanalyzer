##Tag Finder 

### Description
Tag finder is a tool which finds element by and id in original file and trying to search for similar element in sample file

### Installation
Use gradle to build the fat jar. 

###Usage
```bash
java -jar tagfinder-1.0.jar <input_origin_file_path> <input_other_sample_file_path>
```
By default tagfinder searching for element with id "make-everything-ok-button" in original file. 
Than it extracts html attributes form found element and  based on them search sample file with 60% similarity level.

For example:
There is element ```<a id="make-everything-ok-button" class="btn btn-success" title="Make-Button" href="#ok">``` Make everything OK </a>" in original.html and  ```<a class="btn btn-success" href="#done" title="Make-Button">``` in sample.html
Application finds target element in origin.html file and build query based on it's attributes(class, title, href. Note: attribute id and text is not included). 
Using that query it selects all elements with same tag and set of attributes from smaple.html. Next step is to calculate similarity.
In particular case two elements (class, tile) are similar and one (href) is different which is 66% similarity. It's above the default similarity level and the element will be printed.

It is possible to configure target is and similarity level:
```bash
java -jar tagfinder-1.0.jar <input_origin_file_path> <input_other_sample_file_path> -DtargetId=new-id -DsimilarityLevel=80
```
Note that similarity level attribute should be in the rage for 0..100, where 
0 - means all elements with same amount of attributes will be matched
100 - only identical elements will be matched.


