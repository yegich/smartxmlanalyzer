package smart.xml.analyzer;

import org.jsoup.nodes.Element;

import java.util.stream.Collectors;

public class CssQueryExtractor {
    private static final String ID_ATTRIBUTE = "id";

    public String extract(Element element) {
        return element.tag().getName() + element.attributes().asList().stream()
                .filter(a -> !a.getKey().equals(ID_ATTRIBUTE))
                .map(a -> "[" + a.getKey() + "]")
                .collect(Collectors.joining());
    }
}
