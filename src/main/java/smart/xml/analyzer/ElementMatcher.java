package smart.xml.analyzer;

import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElementMatcher {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElementMatcher.class);
    private static final String ID_ATTRIBUTE = "id";
    private final long similarityLevel;

    public ElementMatcher(long similarityLevel) {
        if (similarityLevel < 0) {
            throw new IllegalArgumentException(String.format("Similarity level %d is less than 0", similarityLevel));
        }
        if (similarityLevel > 100) {
            throw new IllegalArgumentException(String.format("Similarity level %d is more than 100", similarityLevel));
        }
        this.similarityLevel = similarityLevel;
    }


    public boolean match(Element original, Element sample) {
        if (original == null || sample == null) {
            throw new IllegalArgumentException(String.format("Elements should be specified origin = %s sample = %s", original, sample));
        }

        if (original.tag() != null && !original.tag().equals(sample.tag())) {
            LOGGER.info("Tags are not equals original tag {} vs smaple tag {}", original.tag(), sample.tag());
            return false;
        }

        long similarity = calculateSimilarity(original.attributes().size() - 1,
                original.attributes().asList().stream()
                        .filter(e -> !ID_ATTRIBUTE.equals(e.getKey()))
                        .filter(e -> sample.attributes().hasKey(e.getKey()) && e.getValue().equals(sample.attributes().get(e.getKey())))
                        .count());
        LOGGER.info("Similarity original attributes {} vs sample attributes {} is {} percents", original.attributes(), sample.attributes(), similarity);

        if (similarity >= similarityLevel) {
            LOGGER.info("Sample {} is matched with origin {} because similarity {} greater or equals similarity level {} ", sample, original, similarity, similarityLevel);
            return true;
        }

        return false;
    }

    private long calculateSimilarity(long size, long matchedCount) {
        return matchedCount * 100 / size;
    }
}
