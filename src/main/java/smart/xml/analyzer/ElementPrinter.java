package smart.xml.analyzer;

import org.jsoup.nodes.Element;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ElementPrinter {

    public String print(Element element) {
        if(element == null) {
            return "No Element";
        }
        List<String> path =  new ArrayList<>();

        postOrderTraversal(element, path);
        return path.stream().collect(Collectors.joining(">"));
    }

    private void postOrderTraversal(Element element, List<String> path) {
        if(element.hasParent()) {
            postOrderTraversal(element.parent(), path);
        }
        if(element.siblingIndex() > 0) {
            path.add(String.format("%s[%d]", element.tagName(), element.siblingIndex()));
        } else {
            path.add(element.tagName());
        }
    }
}
