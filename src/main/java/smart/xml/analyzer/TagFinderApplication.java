package smart.xml.analyzer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.nio.file.Paths;

public class TagFinderApplication {

    public static void main(String [] args) {
        if(args.length < 2) {
            System.out.println("Please specify paths to original and sample files");
        }
        String originalFilePath = args[0];
        String sampleFilePath = args[1];

        String targetId = System.getProperty("targetId") == null ? "make-everything-ok-button" : System.getProperty("targetId");
        long similarityLevel = System.getProperty("similarityLevel") == null ? 60 : Long.valueOf(System.getProperty("similarityLevel"));

        ElementFinder finder = new ElementFinder();
        CssQueryExtractor queryExtractor = new CssQueryExtractor();
        ElementMatcher elementMatcher = new ElementMatcher(similarityLevel);
        ElementPrinter printer = new ElementPrinter();

        Element origin = finder.findElementById(Paths.get(originalFilePath).toFile(), targetId).orElseThrow(RuntimeException::new);
        String query = queryExtractor.extract(origin);
        Elements samples = finder.findElementsByQuery(Paths.get(sampleFilePath).toFile(), query).orElseThrow(RuntimeException::new);

        Element e = samples.stream().filter(s -> elementMatcher.match(origin, s)).findFirst().orElse(null);
        System.out.println(printer.print(e));
    }
}
