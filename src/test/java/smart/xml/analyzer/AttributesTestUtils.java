package smart.xml.analyzer;

import org.jsoup.nodes.Attributes;

import java.util.Map;

public class AttributesTestUtils {

    public static Attributes toAttributes(Map<String, String> attrs) {
        Attributes attributes = new Attributes();
        attrs.entrySet().forEach(e -> attributes.put(e.getKey(), e.getValue()));
        return attributes;
    }
}
