package smart.xml.analyzer;

import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.junit.Test;

import java.util.Map;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static smart.xml.analyzer.AttributesTestUtils.toAttributes;

public class CssQueryExtractorTest {

    private static final Tag DIV_TAG = Tag.valueOf("div");
    private static final Attributes ATTRIBUTES = toAttributes(Map.of(
            "id", "make-everything-ok-button",
            "class", "testClass",
            "href", "#check-and-ok",
            "title", "Make-Button"));

    private CssQueryExtractor extractor = new CssQueryExtractor();

    @Test
    public void extractCssQueryFromElement() {
        String actual = extractor.extract(new Element(DIV_TAG, "", ATTRIBUTES));
        assertThat(actual, containsString("[title]"));
        assertThat(actual, containsString("[class]"));
        assertThat(actual, containsString("[href]"));
        assertThat(actual, not(containsString("[id]")));
    }
}