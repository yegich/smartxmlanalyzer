package smart.xml.analyzer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class ElementFinderTest {

    private static final String ORIGIN_HTML = "sample-0-origin.html";
    private static final String TARGET_ELEMENT_ID = "make-everything-ok-button";

    private ElementFinder finder = new ElementFinder();

    @Test
    public void findExistingId() throws URISyntaxException {
        Optional<Element> target = finder.findElementById(Paths.get(getClass().getResource(ORIGIN_HTML).toURI()).toFile(), TARGET_ELEMENT_ID);
        assertTrue(target.isPresent());
    }

    @Test
    public void findElementsByCssSelector() throws URISyntaxException {
        Optional<Elements> target = finder.findElementsByQuery(Paths.get(getClass().getResource(ORIGIN_HTML).toURI()).toFile(), "a[class][data-toggle]");
        assertThat(target.map(Elements::size).get(), is(4));
    }
}