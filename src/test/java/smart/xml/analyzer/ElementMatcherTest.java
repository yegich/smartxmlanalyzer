package smart.xml.analyzer;

import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static smart.xml.analyzer.AttributesTestUtils.toAttributes;

public class ElementMatcherTest {

    private static final Tag TAG = Tag.valueOf("a");
    private static final Tag DIV_TAG = Tag.valueOf("div");
    private static final Attributes ORIGIN_ATTRIBUTES = toAttributes(Map.of(
            "id", "make-everything-ok-button",
            "class", "testClass",
            "href", "#check-and-ok",
            "title", "Make-Button"));
    private static final Attributes SIMILAR_SAMPLE_ATTRIBUTES = toAttributes(Map.of(
            "class", "testClass",
            "href", "#check-and-ok",
            "title", "Make-Button"));
    private static final Attributes SIMILAR_66PCT_SAMPLE_ATTRIBUTES = toAttributes(Map.of(
            "class", "testClass",
            "href", "#check-and-ok",
            "title", "Do-Button"));
    private static final Attributes SIMILAR_33PCT_SAMPLE_ATTRIBUTES = toAttributes(Map.of(
            "class", "testClass",
            "href", "#check-and-do",
            "title", "Do-Button"));
    private static final String BASE_URI = "/";
    private static final Element ORIGINAL = new Element(TAG, BASE_URI, ORIGIN_ATTRIBUTES);
    private static final Element SIMILAR_SAMPLE = new Element(TAG, BASE_URI, SIMILAR_SAMPLE_ATTRIBUTES);
    private ElementMatcher elementMatcher = new ElementMatcher(66);

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionWhenElementsAreNull() {
        elementMatcher.match(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionWhenOriginIsNull() {
        elementMatcher.match(new Element(TAG, BASE_URI), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionWhenSample() {
        elementMatcher.match(null, new Element(TAG, BASE_URI));
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionIfSimilarityLevelLessThan0() {
        new ElementMatcher(-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void throwExceptionIfSimilarityLevelMoreThan100() {
        new ElementMatcher(-1);
    }

    @Test
    public void doNotMatchElementsWithDifferentTags() {
        assertFalse(elementMatcher.match(ORIGINAL, new Element(DIV_TAG, BASE_URI, ORIGIN_ATTRIBUTES)));
    }

    @Test
    public void skipAttributeIdFromMatching() {
        assertTrue(elementMatcher.match(ORIGINAL, SIMILAR_SAMPLE));
    }

    @Test
    public void matchWith100PctSimilarity() {
        assertTrue(elementMatcher.match(ORIGINAL, ORIGINAL));
    }

    @Test
    public void matchWith70PctSimilarity() {
        assertTrue(elementMatcher.match(ORIGINAL, new Element(TAG, BASE_URI, SIMILAR_66PCT_SAMPLE_ATTRIBUTES)));
    }

    @Test
    public void notMatchWith33PctSimilarity() {
        assertFalse(elementMatcher.match(ORIGINAL, new Element(TAG, BASE_URI, SIMILAR_33PCT_SAMPLE_ATTRIBUTES)));
    }

}