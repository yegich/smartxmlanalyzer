package smart.xml.analyzer;

import org.jsoup.nodes.Element;
import org.jsoup.parser.Tag;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ElementPrinterTest {


    private static final String BASE_URI = "/";
    private ElementPrinter printer = new ElementPrinter();

    @Test
    public void returnNoElementWhenElementNull() {
        assertThat(printer.print(null), is("No Element"));
    }

    @Test
    public void printElementPath() {
        Element html = new Element(Tag.valueOf("html"), BASE_URI);
        Element div = new Element(Tag.valueOf("div"), BASE_URI);
        Element div_1 = new Element(Tag.valueOf("div"), BASE_URI);
        Element a = new Element(Tag.valueOf("a"), BASE_URI);
        Element a_1 = new Element(Tag.valueOf("a"), BASE_URI);
        Element a_2 = new Element(Tag.valueOf("a"), BASE_URI);
        html.insertChildren(0, div, div_1);
        div_1.insertChildren(0,a, a_1, a_2);
        assertThat(printer.print(a_2), is("html>div[1]>a[2]"));
    }
}