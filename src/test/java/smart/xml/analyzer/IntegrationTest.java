package smart.xml.analyzer;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;

public class IntegrationTest {


    private static final String TARGET_ELEMENT_ID = "make-everything-ok-button";
    private static final String ORIGIN_HTML = "sample-0-origin.html";
    private static final String SAMPLE_1_HTML = "sample-1-evil-gemini.html";

    private ElementFinder finder = new ElementFinder();
    private CssQueryExtractor queryExtractor = new CssQueryExtractor();
    private ElementMatcher elementMatcher = new ElementMatcher(60);
    private ElementPrinter printer = new ElementPrinter();

    @Test
    public void verifyCasesFromRequirementsWithSimilarityLevel60pct() throws URISyntaxException {
        Element origin = finder.findElementById(Paths.get(getClass().getResource(ORIGIN_HTML).toURI()).toFile(), TARGET_ELEMENT_ID).orElseThrow(RuntimeException::new);
        String query = queryExtractor.extract(origin);
        Elements samples = finder.findElementsByQuery(Paths.get(getClass().getResource(SAMPLE_1_HTML).toURI()).toFile(), query).orElseThrow(RuntimeException::new);

        Element e = samples.stream().filter(s -> elementMatcher.match(origin, s)).findFirst().get();
        System.out.println(printer.print(e));
    }
}
